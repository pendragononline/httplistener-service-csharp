﻿using System.ServiceProcess;
using System.Windows.Forms;

namespace httplistener_service
{
    public partial class Service1 : ServiceBase
    {
        public Service1()
        {
            InitializeComponent();
        }

        public void OnDebug()
        {
            OnStart(null);
        }

        public static class global
        {
            public static string appendText;
        }

        protected override void OnStart(string[] args)
        {
            WebService.StartWebServer();
        }

        protected override void OnStop()
        {
            Application.Exit();
            WebService.StopWebServer();
        }
    }
}



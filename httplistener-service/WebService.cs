﻿using System;
using System.Text;
using System.Threading.Tasks;
using Newtonsoft.Json;
using System.Windows.Forms;
using System.Net;
using System.IO;
using System.Drawing.Printing;
using System.Management;
using Spire.Pdf;
using System.Diagnostics;
using System.IO.Ports;
using System.Linq;
using System.Threading;
using System.Text.RegularExpressions;

namespace httplistener_service
{
    internal static class WebService
    {
        private const int Port = 8888;
        public const string versionNo = "1.4.9";
        public static string details;
        public static bool errorState;
        public static bool handled;
        public static string absolutePath;
        public static SerialPort port;
        private static readonly HttpListener Listener = new HttpListener { Prefixes = { $"http://127.0.0.1:{Port}/" } };
        private static bool _keepGoing = true;
        private static Task _mainLoop;

        public static void StartWebServer()
        {
            if (_mainLoop != null && !_mainLoop.IsCompleted) return; //Already started
            _mainLoop = MainLoop();
            string eventToLog = "Process started";
            addToLog(eventToLog);
        }

        public static void StopWebServer()
        {
            _keepGoing = false;
            lock (Listener)
            {
                Listener.Stop();
                string eventToLog = "Process stopping, completing current task";
                addToLog(eventToLog);
            }
            try
            {
                _mainLoop.Wait();
                string eventToLog = "Process stopped";
                addToLog(eventToLog);
            }
            catch (Exception e)
            {
                string eventToLog = "Cannot stop process correctly. Error: " + e;
                addToLog(eventToLog);
            }
        }

        private static async Task MainLoop()
        {
            Listener.Start();
            while (_keepGoing)
            {
                try
                {
                    var context = await Listener.GetContextAsync();
                    ProcessRequest(context);
                    lock (Listener)
                    {
                        if (_keepGoing) ProcessRequest(context);
                    }
                }
                catch (Exception e)
                {
                    if (e is HttpListenerException) return; //this gets thrown when the listener is stopped
                }
            }
        }

        private static void ProcessRequest(HttpListenerContext context)
        {
            string eventToLog;
            using (var response = context.Response)
            {
                var body = context.Request.InputStream;
                var reader = new StreamReader(body, context.Request.ContentEncoding);
                var json = reader.ReadToEnd();
                dynamic dynamicResultObject = JsonConvert.DeserializeObject(json);

                try
                {
                    if (context.Request.Url.AbsolutePath == "/print") //PRITNING
                    {
                        absolutePath = "print";
                        if (context.Request.HttpMethod == "POST")
                        {
                            string labelFormat = dynamicResultObject.labelFormat;
                            string labelData = dynamicResultObject.labelData;
                            string labelPrinter = dynamicResultObject.labelPrinter;
                            string labelWidthReceived = dynamicResultObject.labelWidth;
                            string labelHeightReceived = dynamicResultObject.labelHeight;
                            string labelCopiesReceived = dynamicResultObject.labelCopies;

                            int labelHeight;
                            int labelWidth;
                            int labelCopies;

                            if (int.TryParse(labelWidthReceived, out labelHeight) == false)
                            {
                                eventToLog = "Width \'" + labelWidthReceived + "\' is not valid. The label width must be specified as an integer (a whole number). The label will not be printed.";
                                errorState = true;
                                sendStatus(context, eventToLog);
                            }
                            else if (int.TryParse(labelHeightReceived, out labelWidth) == false)
                            {
                                eventToLog = "Height \'" + labelHeightReceived + "\' is not valid. The label height must be specified as an integer (a whole number). The label will not be printed.";
                                errorState = true;
                                sendStatus(context, eventToLog);
                            }

                            else if (int.TryParse(labelCopiesReceived, out labelCopies) == false)
                            {
                                eventToLog = "Copies \'" + labelCopiesReceived + "\' is not valid. The number of copies must be specified as an integer (a whole number). The label will not be printed.";
                                errorState = true;
                                sendStatus(context, eventToLog);
                            }

                            else if (labelCopies == 0)
                            {
                                eventToLog = "0 copies specified. The label will not be printed.";
                                errorState = true;
                                sendStatus(context, eventToLog);
                            }

                            else if ((labelFormat == "pdf" || labelFormat == "zpl") == false)
                            {
                                eventToLog = "Format \'*." + labelFormat + "\' is not supported. Use PDF or ZPL. The label will not be printed.";
                                errorState = true;
                                sendStatus(context, eventToLog);
                            }
                            else if (labelData == "")
                            {
                                eventToLog = "No data to print was received.";
                                errorState = true;
                                sendStatus(context, eventToLog);
                            }
                            else
                            {
                                findPrinter(context, labelFormat, labelWidth, labelHeight, labelData, labelPrinter, labelCopies);
                            }
                        }

                        if (context.Request.HttpMethod == "GET" || context.Request.HttpMethod == "OPTIONS")
                        {
                            response.Headers.Add("Access-Control-Allow-Origin", "*");
                            response.Headers.Add("Access-Control-Allow-Methods", "POST, GET, OPTIONS, PUT, DELETE");
                            response.Headers.Add("Access-Control-Allow-Credentials", "true");
                            response.Headers.Add("Access-Control-Allow-Headers", "Authorization, Content-Type");
                            response.Headers.Add("Vary", "Origin");
                            response.ContentType = "application/json";

                            response.StatusCode = 200;
                            var buffer = Encoding.UTF8.GetBytes("{ \"Status\": \"success\" }");
                            response.ContentLength64 = buffer.Length;
                            response.OutputStream.Write(buffer, 0, buffer.Length);

                            handled = true;
                        }
                    }

                    if (context.Request.Url.AbsolutePath == "/status") //PRINTER STATUS
                    {
                        absolutePath = "status";

                        if (context.Request.HttpMethod == "POST")
                        {
                            string labelPrinter = dynamicResultObject.labelPrinter;
                            string labelData = ""; //set these as nulls since they're not needed for the 'status' request
                            string labelFormat = "";
                            int labelWidth = 0;
                            int labelHeight = 0;
                            int labelCopies = 0;
                            findPrinter(context, labelFormat, labelWidth, labelHeight, labelData, labelPrinter, labelCopies);
                        }

                        if (context.Request.HttpMethod == "GET" || context.Request.HttpMethod == "OPTIONS")
                        {
                            response.Headers.Add("Access-Control-Allow-Origin", "*");
                            response.Headers.Add("Access-Control-Allow-Methods", "POST, GET, OPTIONS, PUT, DELETE");
                            response.Headers.Add("Access-Control-Allow-Credentials", "true");
                            response.Headers.Add("Access-Control-Allow-Headers", "Authorization, Content-Type");
                            response.Headers.Add("Vary", "Origin");
                            response.ContentType = "application/json";

                            response.StatusCode = 200;
                            var buffer = Encoding.UTF8.GetBytes("{ \"Status\": \"success\" }");
                            response.ContentLength64 = buffer.Length;
                            response.OutputStream.Write(buffer, 0, buffer.Length);

                            handled = true;
                        }
                    }

                    else if (context.Request.Url.AbsolutePath == "/scales") //SCALES DATA
                    {

                        absolutePath = "scales";

                        if (context.Request.HttpMethod == "POST")
                        {
                            string comPortReceived = dynamicResultObject.comPort;
                            string baudRateReceived = dynamicResultObject.baudRate;

                            int comPort; //used just to test if the COM port is an int - string is used throughout program since it the Win32 DB is queried 
                            int baudRate;

                            if (int.TryParse(comPortReceived, out comPort) == false)
                            {
                                eventToLog = "The specified COM port \'COM" + comPortReceived + "\' is not valid. The COM port number must be an integer, e.g. for COM1 use 1, for COM2 use 2.";
                                errorState = true;
                                sendStatus(context, eventToLog);
                            }
                            if (int.TryParse(baudRateReceived, out baudRate) == false)
                            {
                                eventToLog = "The specified baud rate \'" + baudRateReceived + "\' is not valid. The baud rate must be an integer (a whole number) and contain no special characters, e.g. 9600.";
                                errorState = true;
                                sendStatus(context, eventToLog);
                            }
                            else
                            {
                                findCOMPorts(context, comPortReceived, baudRate);
                            }
                        }

                        if (context.Request.HttpMethod == "GET" || context.Request.HttpMethod == "OPTIONS")
                        {
                            response.Headers.Add("Access-Control-Allow-Origin", "*");
                            response.Headers.Add("Access-Control-Allow-Methods", "POST, GET, OPTIONS, PUT, DELETE");
                            response.Headers.Add("Access-Control-Allow-Credentials", "true");
                            response.Headers.Add("Access-Control-Allow-Headers", "Authorization, Content-Type");
                            response.Headers.Add("Vary", "Origin");
                            response.ContentType = "application/json";

                            response.StatusCode = 200;
                            var buffer = Encoding.UTF8.GetBytes("{ \"Status\": \"success\" }");
                            response.ContentLength64 = buffer.Length;
                            response.OutputStream.Write(buffer, 0, buffer.Length);

                            handled = true;
                        }
                    }
                }
                catch (Exception e)
                {
                    response.StatusCode = 500;
                    response.ContentType = "application/json";
                    var buffer = Encoding.UTF8.GetBytes(JsonConvert.SerializeObject(e));
                    response.ContentLength64 = buffer.Length;
                    response.OutputStream.Write(buffer, 0, buffer.Length);
                    eventToLog = "Error 500 - general fault. The specific error is: " + e;
                    addToLog(eventToLog);
                    handled = true;
                }
            }
        }

        static void printProps(ManagementObject o, string prop)
        {
            try
            {
                details = prop + "|" + o[prop];
            }
            catch (Exception e)
            {
                Console.Write(e.ToString());
            }
        }

        private static void findPrinter(HttpListenerContext context, string labelFormat, int labelWidth, int labelHeight, string labelData, string labelPrinter, int labelCopies)
        {
            bool useDefault = false;
            if (labelPrinter.Contains("\\")) //check for shared printer
            {
                silentPrint(context, useDefault, labelWidth, labelHeight, labelFormat, labelData, labelCopies, labelPrinter);
            }

            ManagementObjectSearcher findPrinter = new ManagementObjectSearcher("SELECT * FROM Win32_Printer where Name=\'" + labelPrinter + "\'");
            if (findPrinter.Get().Count == 0) //printer specified isn't installed, search for default printer instead
            {
                findPrinter = new ManagementObjectSearcher("SELECT * FROM Win32_Printer " + "Where Location = \'steve\'");
                if (findPrinter.Get().Count == 0) //no default printer found
                {
                    string eventToLog = "No default printers or suitable printers were found. Ensure that a printer is installed and set as default on the client. The label will not be printed.";
                    errorState = true;
                    sendStatus(context, eventToLog);
                }
                else //use default printer
                {
                    foreach (ManagementObject printer in findPrinter.Get())
                    {
                        labelPrinter = printer["Name"].ToString(); //set labelPrinter to the name of the detected default printer
                        useDefault = true;
                        checkPrinterIsActive(context, findPrinter, useDefault, labelData, labelFormat, labelHeight, labelWidth, labelPrinter, labelCopies);
                    }
                }
            }
            else //specified printer found in database
            {
                checkPrinterIsActive(context, findPrinter, useDefault, labelData, labelFormat, labelHeight, labelWidth, labelPrinter, labelCopies);
            }
        }

        private static void checkPrinterIsActive(HttpListenerContext context, ManagementObjectSearcher findPrinter, bool useDefault, string labelData, string labelFormat, int labelWidth, int labelHeight, string labelPrinter, int labelCopies)
        {
            foreach (ManagementObject printer in findPrinter.Get())
            {
                labelPrinter = printer["Name"].ToString();
                printProps(printer, "WorkOffline");

                string eventToLog = "Printer \'" + labelPrinter + "\' is installed on the client.";
                addToLog(eventToLog);

                if (details.Contains("True") == true) //printer is offline
                {
                    eventToLog = "Printer \'" + labelPrinter + "\' is offline or cannot be accessed. The label will not be printed.";
                    errorState = true;
                    sendStatus(context, eventToLog);
                }
                else //printer is active
                {
                    if (absolutePath == "print") //only print if POST request to '/print' is sent
                    {
                        silentPrint(context, useDefault, labelWidth, labelHeight, labelFormat, labelData, labelCopies, labelPrinter);
                    }
                    else if (absolutePath == "status") //if POST request to '/status' is sent
                    {
                        errorState = false;
                        if (useDefault == false)
                        {
                            eventToLog = "Printer \'" + labelPrinter + "\' is installed on the client and is online.";
                        }
                        else
                        {
                            eventToLog = "The specified printer is not online, however printer \'" + labelPrinter + "\' has been detected as the default and is online.";
                        }
                        sendStatus(context, eventToLog);
                    }
                }
            }
        }

        private static void silentPrint(HttpListenerContext context, bool useDefault, int labelWidth, int labelHeight, string labelFormat, string labelData, int labelCopies, string labelPrinter)
        {
            string tempFile = Path.GetTempPath() + Guid.NewGuid().ToString() + ".pdf";
            if (labelFormat == "pdf")
            {
                try
                {
                    byte[] createdPDF = Convert.FromBase64String(labelData); //convert the base64
                    string eventToLog = "Base64 PDF successfully received.";
                    addToLog(eventToLog);

                    try
                    {
                        FileStream stream = new FileStream(tempFile, FileMode.Create); //write received base64 to PDF file
                        BinaryWriter writer = new BinaryWriter(stream);
                        writer.Write(createdPDF, 0, createdPDF.Length);
                        writer.Close();
                    }
                    catch (IOException e)
                    {
                        eventToLog = "Cannot create temporary file or convert the Base64 code. Error: " + e;
                        errorState = true;
                        sendStatus(context, eventToLog);
                    }

                    try
                    {
                        PdfDocument pdfdocument = new PdfDocument();
                        pdfdocument.LoadFromFile(tempFile);

                        PaperSize paper = new PaperSize("Default", labelWidth, labelHeight);
                        paper.RawKind = (int)PaperKind.Custom;
                        pdfdocument.PrintSettings.PaperSize = paper;
                        pdfdocument.PrintSettings.SelectSinglePageLayout(Spire.Pdf.Print.PdfSinglePageScalingMode.FitSize, true);
                        pdfdocument.PrintSettings.PrinterName = labelPrinter;
                        pdfdocument.PrintSettings.Copies = (short)labelCopies;
                        pdfdocument.CompressionLevel = PdfCompressionLevel.None;
                        pdfdocument.PrintSettings.PrintController = new StandardPrintController();
                        pdfdocument.Print();
                        pdfdocument.Dispose();

                        if (useDefault == false)
                        {
                            eventToLog = "PDF successfully printed on printer \'" + labelPrinter + "\'";
                        }
                        else
                        {
                            eventToLog = "PDF successfully printed on printer \'" + labelPrinter + "\' (detected as the default printer)";
                        }

                        errorState = false;
                        sendStatus(context, eventToLog);
                        deleteLabel(context, tempFile);
                    }
                    catch (Exception e)
                    {
                        errorState = true;
                        eventToLog = e.ToString();
                        sendStatus(context, eventToLog);
                    }

                }
                catch (Exception e)
                {
                    string eventToLog = "The PDF could not be printed. Error: " + e;
                    errorState = true;
                    sendStatus(context, eventToLog);
                }
            }

            else //format must be ZPL
            {
                string eventToLog = "";
                try
                {
                    int noOfPrints = 0;

                    while (noOfPrints < labelCopies)
                    {
                        PrintDialog pd = new PrintDialog();
                        pd.PrinterSettings = new PrinterSettings();
                        pd.PrinterSettings.PrinterName = labelPrinter;
                        RawPrinterHelper.SendStringToPrinter(pd.PrinterSettings.PrinterName, labelData);
                        noOfPrints++;
                    }
                    if (useDefault == false)
                    {
                        eventToLog = "ZPL code successfully printed on printer \'" + labelPrinter + "\'";
                    }
                    else
                    {
                        eventToLog = "ZPL code successfully printed on printer \'" + labelPrinter + "\' (detected as the default printer)";
                    }

                    errorState = false;
                    sendStatus(context, eventToLog);
                }
                catch (Exception e)
                {
                    eventToLog = "The ZPL code could not be printed. Error: " + e;
                    errorState = true;
                    sendStatus(context, eventToLog);
                }
            }
        }
        private static void deleteLabel(HttpListenerContext context, string tempFile)
        {
            string eventToLog;
            try
            {
                if (File.Exists(tempFile))
                {
                    File.Delete(tempFile);
                    eventToLog = "Temporary file deleted";
                    addToLog(eventToLog);
                }
                else
                {
                    eventToLog = "Temporary file does not exist, so cannot be deleted";
                    addToLog(eventToLog);
                }

            }
            catch (IOException e)
            {
                eventToLog = "Temporary file cannot be deleted. Error: " + e;
                errorState = true;
                sendStatus(context, eventToLog);
            }
            endOfJob();
        }

        private static void findCOMPorts(HttpListenerContext context, string comPortReceived, int baudRate) //FIND SERIAL PORTS
        {
            string eventToLog = "";
            string[] availableSerialPorts = SerialPort.GetPortNames();
            string portName = "COM" + comPortReceived;

            if (availableSerialPorts.Any(portName.Equals))
            {
                getScalesWeight(context, eventToLog, comPortReceived, baudRate);
            }
            else
            {
                errorState = true;
                eventToLog = "The port \'COM" + comPortReceived + "\' was not found. Ensure that the correct COM port is specified and is installed on the system.";
                sendStatus(context, eventToLog);
            }
        }

        public static void getScalesWeight(HttpListenerContext context, string eventToLog, string comPortReceived, int baudRate)
        {
            port = new SerialPort("COM" + comPortReceived, baudRate, Parity.Even, 7, StopBits.One);
            port.Handshake = Handshake.None;
            port.WriteTimeout = 500;
            port.ReadTimeout = 500;

            try
            {
                port.Open();
                byte[] weightCmd = Encoding.UTF8.GetBytes("H\r");
                port.Write(weightCmd, 0, weightCmd.Length);
                Thread.Sleep(1000);
                portRead(context, eventToLog, port);
            }
            catch (Exception e)
            {
                errorState = true;
                eventToLog = "Cannot connect to serial port. Another program or process may be accessing the port. Error: " + e;
                sendStatus(context, eventToLog);
            }
        }

        private static void portRead(HttpListenerContext context, string eventToLog, SerialPort port)
        {
            while (true)
            {
                int count = port.BytesToRead;
                byte[] ByteArray = new byte[count];
                port.Read(ByteArray, 0, count);
                var receivedData = Encoding.Default.GetString(ByteArray);
                string weightInLb = Regex.Replace(receivedData, @"\t|\n|\r|[ ]{2,}|[^0-9.]", "");

                if (receivedData == "") //assume baud rate is incorrect if no data can be received
                {
                    errorState = true;
                    eventToLog = "No data received from the scales. The baud rate may be incorrect.";
                    portDispose(port);
                    sendStatus(context, eventToLog);
                }
                else
                {
                    double weightInLb_dble = 0;
                    if (double.TryParse(weightInLb, out weightInLb_dble) == false)
                    {
                        errorState = true;
                        portDispose(port);
                        eventToLog = "The weight or data received from the scales is not a number. The item weight cannot be found.";
                        sendStatus(context, eventToLog);
                    }
                    else
                    {
                        errorState = false;
                        double weightInKg = weightInLb_dble / 2.205;
                        double weightInKg_rnd = Math.Round(weightInKg, 3, MidpointRounding.AwayFromZero);
                        eventToLog = weightInKg_rnd.ToString();
                        portDispose(port);
                        sendStatus(context, eventToLog);
                    }
                }
            }
        }

        private static void portDispose(SerialPort port)
        {
            port.DiscardInBuffer();
            port.DiscardOutBuffer();
            port.Dispose();
        }

        private static void addToLog(string eventToLog)
        {
            string logFile = AppDomain.CurrentDomain.BaseDirectory + "log.txt";
            DateTime localDate = DateTime.Now;

            //Create log file if it doesn't exist
            if (!File.Exists(logFile))
            {
                createLogFile(logFile, localDate, eventToLog);
            }
            else
            {
                //Check size of log file (delete if over 10MB)
                FileInfo logFileInfo = new FileInfo(logFile);
                long logFileSize = logFileInfo.Length;

                if (logFileSize > 10000000)
                {
                    try
                    {
                        File.Delete(logFile);
                        eventToLog = eventToLog + "\r\n" + localDate + ": Version " + versionNo + ": INFORMATION! Previous log file deleted on " + localDate + ", this is a new file! Log files are deleted when over 10MB in size.";
                        createLogFile(logFile, localDate, eventToLog);
                    }
                    catch
                    {
                        eventToLog = eventToLog + "\r\n" + localDate + ": Version " + versionNo + ": WARNING! Could not delete previous log file! Continuing writing to current one. Log files should be deleted when over 10MB in size.";
                        writeToLogFile(logFile, localDate, eventToLog);
                    }
                }
                else
                {
                    writeToLogFile(logFile, localDate, eventToLog);
                }
            }
        }

        public static void createLogFile(string logFile, DateTime localDate, string eventToLog)
        {
            // Create a file to write to.
            using (StreamWriter sw = File.CreateText(logFile))
            {
                sw.WriteLine(localDate + ": Version " + versionNo + ": " + eventToLog);
            }
        }

        public static void writeToLogFile(string logFile, DateTime localDate, string eventToLog)
        {
            using (StreamWriter sw = File.AppendText(logFile))
            {
                sw.WriteLine(localDate + ": Version " + versionNo + ": " + eventToLog);
            }
        }

        public static void sendStatus(HttpListenerContext context, string eventToLog)
        {
            using (var response = context.Response)
            {
                try
                {
                    response.Headers.Add("Access-Control-Allow-Origin", "*");
                    response.Headers.Add("Access-Control-Allow-Methods", "POST, GET, OPTIONS, PUT, DELETE");
                    response.Headers.Add("Access-Control-Allow-Credentials", "true");
                    response.Headers.Add("Access-Control-Allow-Headers", "Authorization, Content-Type");
                    response.Headers.Add("Vary", "Origin");
                    response.ContentType = "application/json";

                    var buffer = Encoding.UTF8.GetBytes("content"); //define buffer with psuedo content, overwritten in ifs
                    EventLog eventLog = new EventLog("Application"); //define event logger
                    eventLog.Source = "Application";

                    if (errorState == true) //there was an error
                    {
                        response.StatusCode = 500;
                        buffer = Encoding.UTF8.GetBytes("{\"Version\": \"" + versionNo + "\",\n\"Status\": \"failure" + "\",\n\"Error\": \"" + eventToLog + "\"}");
                        eventLog.WriteEntry(eventToLog, EventLogEntryType.Error, 101, 1);
                    }
                    else //job was successful
                    {
                        response.StatusCode = 200;
                        buffer = Encoding.UTF8.GetBytes("{\"Version\": \"" + versionNo + "\",\n\"Status\": \"success" + "\",\n\"Message\": \"" + eventToLog + "\"}");
                        eventLog.WriteEntry(eventToLog, EventLogEntryType.Information, 101, 1);
                    }
                    response.ContentLength64 = buffer.Length;
                    response.OutputStream.Write(buffer, 0, buffer.Length);
                    handled = true;
                    addToLog(eventToLog);
                    endOfJob();
                }

                catch (Exception e)
                {
                    response.StatusCode = 500;
                    response.ContentType = "application/json";
                    var buffer = Encoding.UTF8.GetBytes(JsonConvert.SerializeObject(e));
                    response.ContentLength64 = buffer.Length;
                    response.OutputStream.Write(buffer, 0, buffer.Length);
                    eventToLog = "Error 500 - general error. The specific error is: " + e;
                    addToLog(eventToLog);
                    endOfJob();
                }
            }
        }
        private static void endOfJob()
        {
            string eventToLog = "----------End of job----------";
            addToLog(eventToLog);
        }
    }
}

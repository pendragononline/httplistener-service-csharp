﻿using Microsoft.Win32;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Diagnostics;
using System.Drawing;
using System.Net;
using System.Text;
using System.Windows.Forms;

namespace Windows_7_Patch
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            try
            {
                RegistryKey myKey = Registry.LocalMachine.OpenSubKey("SYSTEM\\CurrentControlSet\\services\\Spooler", true);
                if (myKey != null)
                {
                    myKey.SetValue("DependOnService", "RPCSS", RegistryValueKind.String);
                    myKey.Close();
                    shutDownDialog();
                }
                else
                {
                    DialogResult keyDoesNotExist = MessageBox.Show("The spooler key does not exist. Would you like to create a patched copy of it?", "Key does not exist", MessageBoxButtons.YesNo, MessageBoxIcon.Exclamation);
                    if (keyDoesNotExist == DialogResult.Yes) //create key
                    {
                        RegistryKey spoolerKey = Registry.LocalMachine.CreateSubKey("SYSTEM\\CurrentControlSet\\services\\Spooler");
                        spoolerKey.SetValue("DependOnService", "RPCSS");
                        spoolerKey.Close();
                        shutDownDialog();
                    }
                    else
                    {
                        Application.Exit();
                    }
                }
            }
            catch (Exception err)
            {
                string statusCode = "c";
                updateStatusLabel(statusCode);
                MessageBox.Show("Error. The operation could not be completed.\n\nError: " + err, "The operation could not be completed", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void updateStatusLabel(string statusCode)
        {
            if (statusCode == "a") //default
            {
                statusLabel.Text = "Patching Windows 7 Spooler.";
            }
            if (statusCode == "b")
            {
                statusLabel.Text = "Patched. Reboot computer.";
            }
            if (statusCode == "c")
            {
                statusLabel.Text = "Error. Could not patch.";
            }
        }

        private void shutDownDialog()
        {
            string statusCode = "b";
            updateStatusLabel(statusCode);
            DialogResult successfulPatch = MessageBox.Show("Operation completed successfully. Press OK to reboot your computer, otherwise press Cancel.", "Operation successful", MessageBoxButtons.OKCancel, MessageBoxIcon.Information);
            if (successfulPatch == DialogResult.OK)
            {
                StartShutDown("-f -r -t 3"); //reboot Windows
            }
            else
            {
                Application.Exit();
            }
        }

        //Shutdown command
        private static void StartShutDown(string param)
        {
            ProcessStartInfo proc = new ProcessStartInfo();
            proc.FileName = "cmd";
            proc.WindowStyle = ProcessWindowStyle.Hidden;
            proc.Arguments = "/C shutdown " + param;
            Process.Start(proc);
        }
    }
}

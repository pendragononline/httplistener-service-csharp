﻿using System;
using System.IO;
using System.ServiceProcess;
using System.Threading;
using System.Windows.Forms;

namespace httplistener_service
{
    public partial class Service1 : ServiceBase
    {
        public Service1()
        {
            InitializeComponent();
        }

        public void OnDebug()
        {
            OnStart(null);
        }

        public static class global
        {
            public static string appendText;
        }

        protected override void OnStart(string[] args)
        {
            while (!System.Diagnostics.Debugger.IsAttached) Thread.Sleep(100);
            try
            {
                WebService.StartWebServer();
            }
            catch (Exception e)
            {
                string eventToLog = e.ToString();
                addToLogStart(eventToLog);
            }
        }

        protected override void OnStop()
        {
            Application.Exit();
            WebService.StopWebServer();
        }

        private static void addToLogStart(string eventToLog)
        {
            var logFile = AppDomain.CurrentDomain.BaseDirectory + "log-start.txt";
            DateTime localDate = DateTime.Now;

            //Create log file if it doesn't exist
            if (!File.Exists(logFile))
            {
                // Create a file to write to.
                using (StreamWriter sw = File.CreateText(logFile))
                {
                    sw.WriteLine(localDate + ": " + eventToLog);
                }
            }
            else
            {
                using (StreamWriter sw = File.AppendText(logFile))
                {
                    sw.WriteLine(localDate + ": " + eventToLog);
                }
            }
        }
    }
}


